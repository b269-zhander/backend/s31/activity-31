// Create Server


let http = require("http");

const port = 3000;

const server = http.createServer((request, response)=> {

	if( request.url == '/greeting' ){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Server Running!");
	} else if (request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("User Login Page!");
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Error: Page not available!");
	}

});

server.listen(port);
console.log(`Server now accessible at localhost: ${port}`);